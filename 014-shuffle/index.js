export function countShuffle(numberCards) {
    if (numberCards < 2) throw new Error('Number must be greater than 2');
    if (numberCards % 2 !== 0) throw new Error('Number must be even')
    const deck = createArray(numberCards)
    let shuffledDeck = shuffle(deck)
    let count = 1;
    // Second number will never be in the same place until final shuffle
    while (shuffledDeck[1] !== deck[1]) {
        shuffledDeck = shuffle(shuffledDeck)
        count++
    }
    return count
}
function shuffle(deck) {
    let mid = Math.ceil((deck.length) / 2)
    let first = deck[0] // Keep first in same place
    let left = deck.slice(1, mid), right = deck.slice(mid)
    /* swap alternating
    * left 2 3 4
    * right 5 6 7 8
    * want 5 2 6 3 7 4 8
    * == right[0] left[0] right[1] left[1] etc 
    */
   let newArr = [first]
   for (let i = 0; i < Math.min(left.length, right.length); i++) {
       newArr.push(right[i])
       newArr.push(left[i])
    }
    if (left.length > right.length) newArr.push(left[left.length - 1])
    if (right.length > left.length) newArr.push(right[right.length - 1])
    // console.log(newArr.join(' '))
    return newArr
}
function createArray(numberCards) {
    let arr = []
    for (let num = 0; num < numberCards; num++) arr.push(num) 
    return arr
}