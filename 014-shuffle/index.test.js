import { countShuffle } from ".";
test("8 -> 3", () => {
    const input = 8;
    const expected = 3;

    const actual = countShuffle(input)
    expect(actual).toBe(expected)
})
test("52 -> 8", () => {
    const input = 52;
    const expected = 8;

    const actual = countShuffle(input)
    expect(actual).toBe(expected)
})
test("14 -> 12", () => {
    const input = 14;
    const expected = 12;

    const actual = countShuffle(input)
    expect(actual).toBe(expected)
})
test("3 -> Number must be even", () => {
    expect(() => countShuffle(3)).toThrow('Number must be even')
})
test("0 -> Number must be greater than 2", () => {
    const input = 0
    const expected = 'Number must be greater than 2'
    expect(() => countShuffle(input)).toThrow(expected)
})