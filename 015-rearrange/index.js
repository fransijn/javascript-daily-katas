export function rearrange(sentence)
{
    if (sentence.length == 1) return ''
    if (!isNaN(sentence)) throw new Error ('Input must be a valid string')
    const words = sentence.split(' ')
    // Dictionary to save places for words
    let nums = {}
    for (const word of words)
    {
        const place = findPosition(word)
        const wordIndex = word.indexOf(place)
        // Remove number from word
        let newW = word.substring(0, word.indexOf(place))
        if (wordIndex !== word.length - 1) newW += word.substring(wordIndex + 1)
        nums[place] = newW
    }
    return placeLetters(nums)
}
// Function that returns the position found in word
function findPosition(words)
{
    const regEx = /[1-9]/
    for (const letter of words)
    {
        if (letter.match(regEx)) 
            return letter
    }
}
// Final result function
function placeLetters(places)
{
    let outp = ''
    Object.keys(places).forEach(place => {
        let word = places[place];
        outp += word + ' '
    })
    return outp.trimEnd()
}
