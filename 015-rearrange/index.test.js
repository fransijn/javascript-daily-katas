import { rearrange } from ".";
test('is2 Thi1s T4est 3a -> This is a Test', () => {
    const input = 'is2 Thi1s T4est 3a'
    const expected = 'This is a Test'

    const actual = rearrange(input)
    expect(actual).toBe(expected)
})
test('4of Fo1r pe6ople g3ood th5e the2 -> For the good of the people', () => {
    const input = '4of Fo1r pe6ople g3ood th5e the2'
    const expected = 'For the good of the people'

    const actual = rearrange(input)
    expect(actual).toBe(expected)
})
test('5weird i2s JavaScri1pt dam4n so3 -> JavaScript is so damn weird', () => {
    const input = '5weird i2s JavaScri1pt dam4n so3'
    const expected = 'JavaScript is so damn weird'

    const actual = rearrange(input)
    expect(actual).toBe(expected)
})
test('empty string -> null', () => {
    const input = ' '
    const expected = ''

    const actual = rearrange(input)
    expect(actual).toBe(expected)
})
test('number as input should throw -> Error', () => {
    const input = 123456
    const expected = 'Input must be a valid string'

    expect(() => rearrange(input)).toThrow(expected)
})
