import { XAndO } from ".";
test('[" | | ", " |X| ", "X| | "]', () => {
    const input = [" | | ", " |X| ", "X| | "]
    const expected = [1, 3]

    const actual = XAndO(input)

    expect(actual).toStrictEqual(expected)
}
)
test('XAndO(board = ["X|X|O", "O|X| ", "X|O| "]) ➞ [3, 3]', () => {
    const input = ["X|X|O", "O|X| ", "X|O| "]
    const expected = [3, 3]

    const actual = XAndO(input)

    expect(actual).toStrictEqual(expected)
}
)
test('XAndO(board = ["X|O|O", "O|O| ", "O|O| "]) ➞ false', () => {
    const input = ["X|O|O", "O|O| ", "O|O| "]
    const expected = false

    const actual = XAndO(input)

    expect(actual).toStrictEqual(expected)
}
)
