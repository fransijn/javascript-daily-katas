let emptyInd = 0
export function XAndO(board) {
    const columns = findColumns(board)
    const rows = findRows(board)
    const diagonals = findDiagonals(board)

    const solveRows = canSolveLines(rows);
    const solveColumns = canSolveLines(columns);
    const solveDiagonals = canSolveLines(diagonals);
    if (solveRows !== false) return getSolution(solveRows, 1, emptyInd)
    if (solveDiagonals !== false) return getSolution(solveDiagonals, 0, emptyInd, diagonals.indexOf(solveDiagonals))
    if (solveColumns !== false) return getSolution(solveColumns, -1, emptyInd)
    return false;
}

function findColumns(board) {
    /*  column
        X   -   -
        X   -   -
        X   -   -
    */
    let columns = [[], [], []]
    for (let i = 0; i < 3; i++) // Go through rows
    {
        for (let j = 0; j < 3; j++) {
            let items = board[j].split('|')
            columns[i].push(items[i])
        }
    }
    return columns
}
function findRows(board) {
    /*  rows
        X   X   X
        -   -   -
        -   -   -
    */
    let rows = [[], [], []]
    for (let i = 0; i < 3; i++) {
        let items = board[i].split('|')
        rows[i].push(items)
    }
    return rows;
}
function findDiagonals(board) {
    /*  x1:
        X   -   -
        -   X   -
        -   -   X
        x2:
        -   -   X
        -   X   -
        X   -   -

    */
    let topLeftBottomRight = 0
    let bottomLeftTopRight = 2
    let diagonals = [[], []]
    for (let i = 0; i < 3; i++) {
        let items = board[i].split('|')
        diagonals[0].push(items[topLeftBottomRight])
        diagonals[1].push(items[bottomLeftTopRight])
        topLeftBottomRight++;
        bottomLeftTopRight--;
    }
    return diagonals
}
function getSolution(line, direction, index, diagonal = null) {
    if (direction == 1) // rows
        return [index + 1, line.indexOf(' ') + 1]
    if (direction == -1) // columns
        return [line.indexOf(' ') + 1, index + 1]
    if (direction == 0) // diagonals
    {
        if (diagonal == 0)
            return [line.indexOf(' ') + 1, line.indexOf(' ') + 1]
        if (diagonal == 1)
        {
            return [line.indexOf(' ') + 1, 3 - line.indexOf(' ')]
        }

    }
}
// Check if solvable
function canSolveLines(lines) {
    for (var line of lines) {
        if (line.filter(char => {
            return char === 'X'
        }).length == 2) {
            return line;
        }
    }
    return false;
}
