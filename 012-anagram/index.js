export function anagrams(word, arrayWords) {
    if (!word) throw new Error('No word')
    if (!arrayWords) throw new Error('No array of words')
    if (arrayWords == [] || word == '') throw new Error('Wrong input')
    let out = []
    const wordCount = countLetters(word)
    for (const anagram of arrayWords) {
        const anagramCount = countLetters(anagram)
        if (isAnagram(wordCount, anagramCount)) out.push(anagram)
    }
    return out
}
function countLetters(word) {
    let counts = {}
    for (const letter of word)
        counts[letter] = (counts[letter] == undefined ? 1 : counts[letter] + 1)
    return counts
}
function isAnagram(wordCount, anagramCount) {
    const letters = Object.keys(anagramCount)
    for (const letter of letters) {
        if (wordCount[letter] === undefined || anagramCount[letter] !== wordCount[letter]) return false
    }
    return true
}