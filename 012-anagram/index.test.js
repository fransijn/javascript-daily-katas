import { anagrams } from ".";
test("anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa'] ", () => {
    const word = 'abba'
    const arrayWords = ['aabb', 'abcd', 'bbaa', 'dada']

    const expected = ['aabb', 'bbaa']

    const actual = anagrams(word, arrayWords)
    expect(actual).toStrictEqual(expected)
})
test("anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer'])", () => {
    const word = 'racer'
    const arrayWords = ['crazer', 'carer', 'racar', 'caers', 'racer']

    const expected = ['carer', 'racer']

    const actual = anagrams(word, arrayWords)
    expect(actual).toStrictEqual(expected)
})
test("anagrams('laser', ['lazing', 'lazy',  'lacer'])", () => {
    const word = 'laser'
    const arrayWords = ['lazing', 'lazy',  'lacer']

    const expected = []

    const actual = anagrams(word, arrayWords)
    expect(actual).toStrictEqual(expected)
})
test("anagrams(['aabb', 'aacc'])", () => {
    let word
    let arrayWords = ['aabb', 'aacc']
    expect(() => anagrams(word, arrayWords)).toThrow('No word')
})
test("anagrams(aabb)", () => {
    const word = 'aabb'
    let arrayWords
    expect(() => anagrams(word, arrayWords)).toThrow('No array')
})